package ar.edu.utnfc.argprog.sakila.data.repositories;

import ar.edu.utnfc.argprog.sakila.commons.exceptions.TechnicalException;
import ar.edu.utnfc.argprog.sakila.data.commons.Repository;
import ar.edu.utnfc.argprog.sakila.data.entities.FilmEntity;
import jakarta.persistence.Query;
import org.hibernate.HibernateException;

import java.util.List;

public class FilmRepository extends Repository<FilmEntity, Integer> {

    public List<FilmEntity> findByLanguage(int languageId) {
        try {
            String className = getEntityClass().getSimpleName();
            Query query = entityManager.createQuery("SELECT e FROM FilmEntity e WHERE e.languageEntity1.id = " + languageId);

            return query.getResultList();
        }
        catch (HibernateException ex) {
            throw new TechnicalException(ex);
        }

    }

    public List<FilmEntity> findByLanguageNamed(int filter) {
        try {
            String className = getEntityClass().getSimpleName();
            Query query = entityManager.createNamedQuery("FilmEntity.findByLanguageId")
                    .setParameter("languageId", filter);

            return query.getResultList();
        }
        catch (HibernateException ex) {
            throw new TechnicalException(ex);
        }

    }

}
