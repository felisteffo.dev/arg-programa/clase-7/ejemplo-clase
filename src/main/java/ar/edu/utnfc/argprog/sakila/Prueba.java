/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.edu.utnfc.argprog.sakila;


import java.util.Collection;
import java.util.List;

import ar.edu.utnfc.argprog.sakila.data.commons.LocalEntityManagerProvider;
import ar.edu.utnfc.argprog.sakila.data.entities.FilmEntity;
import ar.edu.utnfc.argprog.sakila.data.entities.LanguageEntity;
import ar.edu.utnfc.argprog.sakila.data.repositories.FilmRepository;
import ar.edu.utnfc.argprog.sakila.data.repositories.LanguageRepository;

/**
 * @author Felipe
 */
public class Prueba {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        System.out.println("\n\nInicio de las pruebas ==>");

        List<LanguageEntity> listaLenguajes = new LanguageRepository()
                .findAll();
        System.out.println("Lista de lenguajes antes del insert");
        listaLenguajes.stream()
                .forEach(l -> {
                    System.out.println(l);
//                    l.getFilmEntityCollection1()
//                            .stream()
//                            .forEach(f -> {
//                                System.out.println("\t" + f);
//                            });
                });

//        List<FilmEntity> listaFilms = new FilmRepository()
//                .findAll();
//        System.out.println("\n\nLista de films");
//        listaFilms.stream()
//                .forEach(System.out::println);


//        Collection<FilmEntity> listaFilmsItaliano = new LanguageRepository()
//                .retrieve(2)
//                .getFilmEntityCollection1();
        List<FilmEntity> listaFilmsItaliano = new FilmRepository()
                .findByLanguageNamed(2);
        System.out.println("\n\nLista de films en italiano");
        listaFilmsItaliano.stream()
                .forEach(System.out::println);


        LocalEntityManagerProvider.closeEntityManager();
    }

}
